package array

import (
	"reflect"
	"sync"
)

// Map an rarray to other array
func Map(array interface{}, callback func(interface{}) interface{}) []interface{} {
	arrayVal := reflect.ValueOf(array)
	switch arrayVal.Kind() {
	case reflect.Array, reflect.Slice:
		len := arrayVal.Len()
		arrayRes := make([]interface{}, len, len)
		mapReflect(arrayVal, arrayRes, len, callback)
		return arrayRes
	default:
		return []interface{}{callback(array)}
	}
}

func mapReflect(in reflect.Value, res []interface{}, len int, callback func(interface{}) interface{}) {
	wait := sync.WaitGroup{}
	wait.Add(len)
	for i := 0; i < len; i++ {
		go func(val interface{}, res []interface{}, i int) {
			res[i] = callback(val)
			wait.Done()
		}(in.Index(i).Interface(), res, i)
	}
	wait.Wait()
}
