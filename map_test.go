package array

import (
	"reflect"
	"testing"
)

func TestMap(t *testing.T) {
	type args struct {
		array    interface{}
		callback func(interface{}) interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Map(tt.args.array, tt.args.callback); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Map() = %v, want %v", got, tt.want)
			}
		})
	}
}
