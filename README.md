# Yet Another Go Parallel Array Functions

Provides parallel ```Each``` and ```Map```

## Basic example

```golang
package main

import (
	"fmt"

	"gitlab.com/guest-one/go/array"
)

func main() {

	data1 := []int{10, 20, 30, 40, 50, 60, 70, 80}

	fmt.Println("Parallel squares:")
	array.Each(data1, func(elem interface{}) {
		x := elem.(int)
		fmt.Printf("Suquare of %d is %d\n", x, x*x)
	})

	fmt.Println("Mapped squares:")
	data2 := array.Map(data1, func(elem interface{}) interface{} {
		x := elem.(int)
		return x * x
	})

	for i := range data1 {
		fmt.Printf("Suquare of %d is %d\n", data1[i], data2[i])
	}
}
```
Expected output:
```
go run main.go 
Parallel squares:
Suquare of 80 is 6400
Suquare of 60 is 3600
Suquare of 10 is 100
Suquare of 30 is 900
Suquare of 50 is 2500
Suquare of 20 is 400
Suquare of 40 is 1600
Suquare of 70 is 4900
Mapped squares:
Suquare of 10 is 100
Suquare of 20 is 400
Suquare of 30 is 900
Suquare of 40 is 1600
Suquare of 50 is 2500
Suquare of 60 is 3600
Suquare of 70 is 4900
Suquare of 80 is 6400
```

## Advanced Example
```golang
package main

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/guest-one/go/array"
)

type result struct {
	Url    string
	Robots string
	Ok     bool
}

func main() {

	sites := []string{"facebook.com", "xkcd.com", "vk.com", "example.com"}

	robots := array.Map(sites, func(elem interface{}) interface{} {
		site := elem.(string)
		var ret result
		ret.Url = "https://" + site + "/robots.txt"
		resp, err := http.Get(ret.Url)
		if err == nil {
			ret.Ok = true
			buf, _ := ioutil.ReadAll(resp.Body)
			ret.Robots = string(buf)
		} else {
			ret.Ok = false
		}
		return ret
	})

	for _, elem := range robots {
		r := elem.(result)
		fmt.Println("A file robots.txt from URL ", r.Url)
		if r.Ok {
			fmt.Println("Have been downloaded. Len = ", len(r.Robots))
		} else {
			fmt.Println("Have not been downloaded")
		}
	}
}
```

Expected output:
```
A file robots.txt from URL  https://facebook.com/robots.txt
Have been downloaded. Len =  6493
A file robots.txt from URL  https://xkcd.com/robots.txt
Have been downloaded. Len =  34
A file robots.txt from URL  https://vk.com/robots.txt
Have been downloaded. Len =  28
A file robots.txt from URL  https://example.com/robots.txt
Have been downloaded. Len =  1270
```
