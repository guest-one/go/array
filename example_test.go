package array_test

import (
	"fmt"

	"gitlab.com/guest-one/go/array"
)

func ExampleEach() {
	data1 := []int{10, 20, 30, 40, 50, 60, 70, 80}

	fmt.Println("Parallel squares:")
	array.Each(data1, func(elem interface{}) {
		x := elem.(int)
		fmt.Printf("Suquare of %d is %d\n", x, x*x)
	})
}

func ExampleMap() {
	data1 := []int{10, 20, 30, 40, 50, 60, 70, 80}

	fmt.Println("Mapped squares:")
	data2 := array.Map(data1, func(elem interface{}) interface{} {
		x := elem.(int)
		return x * x
	})

	for i := range data1 {
		fmt.Printf("Suquare of %d is %d\n", data1[i], data2[i])
	}
}
