package array

import (
	"reflect"
	"sync"
)

// Each calls a callback for every array element
func Each(array interface{}, callback func(interface{})) {
	arrayVal := reflect.ValueOf(array)
	switch arrayVal.Kind() {
	case reflect.Slice, reflect.Array:
		wait := sync.WaitGroup{}
		len := arrayVal.Len()
		wait.Add(len)
		for i := 0; i < len; i++ {
			go func(val interface{}) {
				callback(val)
				wait.Done()
			}(arrayVal.Index(i).Interface())
		}
		wait.Wait()
	default:
		callback(array)
	}
}
