package array_test

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/guest-one/go/array"
)

type result struct {
	Url    string
	Robots string
	Ok     bool
}

func Example() {
	sites := []string{"facebook.com", "xkcd.com", "vk.com", "example.com"}

	robots := array.Map(sites, func(elem interface{}) interface{} {
		site := elem.(string)
		var ret result
		ret.Url = "https://" + site + "/robots.txt"
		resp, err := http.Get(ret.Url)
		if err == nil {
			ret.Ok = true
			buf, _ := ioutil.ReadAll(resp.Body)
			ret.Robots = string(buf)
		} else {
			ret.Ok = false
		}
		return ret
	})

	for _, elem := range robots {
		r := elem.(result)
		fmt.Println("A file robots.txt from URL ", r.Url)
		if r.Ok {
			fmt.Println("Have been downloaded. Len = ", len(r.Robots))
		} else {
			fmt.Println("Have not been downloaded")
		}
	}
}
