package array

import "testing"

func TestEach(t *testing.T) {
	// sainity test
	type args struct {
		array    interface{}
		callback func(interface{})
	}
	testCallback := func(interface{}) {

	}
	tests := []struct {
		name string
		args args
	}{
		{"Integer         ", args{20, testCallback}},
		{"String          ", args{". . . . .", testCallback}},
		{"Sllice of ints  ", args{[]int{1, 2, 3}, testCallback}},
		{"Array of strings", args{[4]string{"a", "b", "c", "d"}, testCallback}},
		{"Struct", args{struct {
			a int
			b float64
		}{10, 5.5}, testCallback}},
		{"Nil pointer     ", args{nil, testCallback}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Each(tt.args.array, tt.args.callback)
		})
	}
	//example
	t.Run("Four sums", func(t *testing.T) {
		out := make(chan (int), 20)
		sum := func(a interface{}) {
			v := a.([2]int)
			out <- v[0] + v[1]
		}
		Each([]([2]int){{1, 3}, {2, 2}, {4, 0}, {-2, 6}}, sum)
		if 4 != <-out {
			t.Error(" != 4")
		}
		if 4 != <-out {
			t.Error(" != 4")
		}
		if 4 != <-out {
			t.Error(" != 4")
		}
		if 4 != <-out {
			t.Error(" != 4")
		}
	})
}
